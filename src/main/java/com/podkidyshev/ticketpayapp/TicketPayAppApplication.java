package com.podkidyshev.ticketpayapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TicketPayAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(TicketPayAppApplication.class, args);
    }

}
