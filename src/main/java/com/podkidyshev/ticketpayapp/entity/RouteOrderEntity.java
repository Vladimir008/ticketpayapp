package com.podkidyshev.ticketpayapp.entity;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "route_order")
public class RouteOrderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "\"from\"", nullable = false)
    private String from;
    @Column(name = "\"to\"", nullable = false)
    private String to;
    @Column(name = "price", nullable = false)
    private Double price;

    @ColumnDefault("CURRENT_TIMESTAMP(6)")
    @Column(name = "buying_date", nullable = false, updatable = false)
    private LocalDateTime buyingDate;

    @ColumnDefault("CURRENT_TIMESTAMP(6)")
    @Column(name = "action_date",nullable = false)
    private LocalDateTime actionDate;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    @PrePersist
    public void  prePersist() {
        this.buyingDate = LocalDateTime.now();
        this.actionDate = LocalDateTime.now();
    }

    /*@PreUpdate
    public void preUpdate(int year, int month, int day, int hour, int min) {
        this.actionDate = LocalDateTime.of(year, month, day, hour, min);
    }*/

}
