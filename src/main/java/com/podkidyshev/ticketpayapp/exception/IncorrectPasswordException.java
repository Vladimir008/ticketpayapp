package com.podkidyshev.ticketpayapp.exception;

public class IncorrectPasswordException extends  Exception{

    public IncorrectPasswordException(String message) {
        super(message);
    }
}
