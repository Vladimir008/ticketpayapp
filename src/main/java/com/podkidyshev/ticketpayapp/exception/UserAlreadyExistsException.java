package com.podkidyshev.ticketpayapp.exception;

public class UserAlreadyExistsException extends  Exception{

    public UserAlreadyExistsException(String message) {
        super(message);
    }
}