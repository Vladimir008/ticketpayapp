package com.podkidyshev.ticketpayapp.model;

import com.podkidyshev.ticketpayapp.entity.UserEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserEditPOJO {
    private String firstName;
    private String lastName;
    private String email;
    private String password;

/*    public static UserPOJO toModel(UserEntity userEntity) {
        UserPOJO userPOJO = new UserPOJO();
        userPOJO.setFirstName(userEntity.getFirstName());
        userPOJO.setLastName(userEntity.getLastName());
        userPOJO.setEmail(userEntity.getEmail());

        return userPOJO;
    }*/
}
