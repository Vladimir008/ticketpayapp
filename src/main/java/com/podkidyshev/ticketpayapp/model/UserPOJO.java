package com.podkidyshev.ticketpayapp.model;

import com.podkidyshev.ticketpayapp.entity.RouteOrderEntity;
import com.podkidyshev.ticketpayapp.entity.UserEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class UserPOJO {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    //private List<RouteOrderPOJO> routes;

    public static UserPOJO toModel(UserEntity userEntity) {
        UserPOJO userPOJO = new UserPOJO();
        userPOJO.setId(userEntity.getId());
        userPOJO.setFirstName(userEntity.getFirstName());
        userPOJO.setLastName(userEntity.getLastName());
        userPOJO.setEmail(userEntity.getEmail());
        //userPOJO.setRoutes(userEntity.getRoutes().stream().map(RouteOrderPOJO::toModel).collect(Collectors.toList()));

        return userPOJO;
    }
}
