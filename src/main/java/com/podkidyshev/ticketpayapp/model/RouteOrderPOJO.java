package com.podkidyshev.ticketpayapp.model;

import com.podkidyshev.ticketpayapp.entity.RouteOrderEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class RouteOrderPOJO {

    private Long id;
    private String from;
    private String to;
    private Double price;
    private LocalDateTime buyingDate;
    private LocalDateTime actionDate;
    private Long userId;

    public static RouteOrderPOJO toModel(RouteOrderEntity routeOrderEntity) {
        RouteOrderPOJO routeOrderPOJO = new RouteOrderPOJO();
        routeOrderPOJO.setId(routeOrderEntity.getId());
        routeOrderPOJO.setFrom(routeOrderEntity.getFrom());
        routeOrderPOJO.setTo(routeOrderEntity.getTo());
        routeOrderPOJO.setPrice(routeOrderEntity.getPrice());
        routeOrderPOJO.setBuyingDate(routeOrderEntity.getBuyingDate());
        routeOrderPOJO.setActionDate(routeOrderEntity.getActionDate());
        routeOrderPOJO.setUserId(routeOrderEntity.getUserEntity().getId());
        return routeOrderPOJO;
    }
}
