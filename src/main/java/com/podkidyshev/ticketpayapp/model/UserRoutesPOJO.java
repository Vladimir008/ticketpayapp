package com.podkidyshev.ticketpayapp.model;

import com.podkidyshev.ticketpayapp.entity.UserEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class UserRoutesPOJO {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private List<RouteOrderPOJO> routes;

    public static UserRoutesPOJO toModel(UserEntity userEntity) {
        UserRoutesPOJO userRoutesPOJO = new UserRoutesPOJO();
        userRoutesPOJO.setId(userEntity.getId());
        userRoutesPOJO.setFirstName(userEntity.getFirstName());
        userRoutesPOJO.setLastName(userEntity.getLastName());
        userRoutesPOJO.setEmail(userEntity.getEmail());
        userRoutesPOJO.setRoutes(userEntity.getRoutes().stream().map(RouteOrderPOJO::toModel).collect(Collectors.toList()));

        return userRoutesPOJO;
    }
}

