package com.podkidyshev.ticketpayapp.repository;

import com.podkidyshev.ticketpayapp.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
    UserEntity findByEmail(String email);
}

