package com.podkidyshev.ticketpayapp.repository;

import com.podkidyshev.ticketpayapp.entity.RouteOrderEntity;
import com.podkidyshev.ticketpayapp.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RouteOrderRepository extends JpaRepository<RouteOrderEntity, Long> {
   void deleteAllByUserEntity(UserEntity userEntity);
}
