package com.podkidyshev.ticketpayapp.controller;

import com.podkidyshev.ticketpayapp.entity.RouteOrderEntity;
import com.podkidyshev.ticketpayapp.exception.UserNotFoundException;
import com.podkidyshev.ticketpayapp.service.RouteOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

@RestController
@RequestMapping(value = "api/v1/routeOrder")
public class RouteOrderController {

    @Autowired
    private RouteOrderService routeOrderService;

    @GetMapping("/all")
    public ResponseEntity getAll() {
       return ResponseEntity.ok(routeOrderService.getAll());
    }

    @GetMapping
    public ResponseEntity getOneOrderById(@RequestParam Long id) {
        try {
            return ResponseEntity.ok(routeOrderService.getOne(id));
        } catch (NoSuchElementException e) {
            return ResponseEntity.badRequest().body("Заказ не найден!");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка! " + e.getMessage());
        }
    }
    @PostMapping("/newOrder")
    public ResponseEntity registration(@RequestBody RouteOrderEntity routeOrderEntity, @RequestParam Long userId) {
        try {
            return ResponseEntity.ok(routeOrderService.createNewOrder(routeOrderEntity, userId));
        } catch (UserNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка! " + e.getMessage());
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity deleteOrderById(@PathVariable Long id) {
        try {
        return ResponseEntity.ok(routeOrderService.delete(id));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка! " + e.getMessage());
        }
    }

    @DeleteMapping("/deleteAll/{userId}")
    public ResponseEntity deleteAllByUserId(@PathVariable Long userId) {
        try {
            return ResponseEntity.ok(routeOrderService.deleteAllByUser(userId));
        }catch (UserNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка! " + e.getMessage());
        }
    }
}
