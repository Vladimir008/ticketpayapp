package com.podkidyshev.ticketpayapp.controller;

import com.podkidyshev.ticketpayapp.entity.UserEntity;
import com.podkidyshev.ticketpayapp.exception.UserAlreadyExistsException;
import com.podkidyshev.ticketpayapp.exception.UserNotFoundException;
import com.podkidyshev.ticketpayapp.model.UserEditPOJO;
import com.podkidyshev.ticketpayapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

@RestController
@RequestMapping(value = "/api/v1/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity getOneUserById(@RequestParam Long id) {
        try {
            return ResponseEntity.ok(userService.getOne(id));
        } catch (UserNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (NoSuchElementException e) {
            return ResponseEntity.badRequest().body("Пользователь не найден!");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка! " + e.getMessage());
        }
    }

    @GetMapping("/all")
    public ResponseEntity findAll() {
        return ResponseEntity.ok(userService.findAll());
    }

    @PostMapping("/newUser")
    public ResponseEntity registration(@RequestBody UserEntity userEntity) {
        try {
            return ResponseEntity.ok(userService.registration(userEntity));
        } catch (UserAlreadyExistsException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка! " + e.getMessage());
        }
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity edit(@PathVariable Long id, @RequestBody UserEditPOJO userEditPOJO) {
        try {
            return ResponseEntity.ok(userService.edit(id, userEditPOJO));
        } catch (UserNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка! " + e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteUser(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(userService.delete(id));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка! " + e.getMessage());
        }
    }
}
