package com.podkidyshev.ticketpayapp.service;

import com.podkidyshev.ticketpayapp.entity.UserEntity;
import com.podkidyshev.ticketpayapp.exception.IncorrectPasswordException;
import com.podkidyshev.ticketpayapp.exception.UserAlreadyExistsException;
import com.podkidyshev.ticketpayapp.exception.UserNotFoundException;
import com.podkidyshev.ticketpayapp.model.UserEditPOJO;
import com.podkidyshev.ticketpayapp.model.UserPOJO;
import com.podkidyshev.ticketpayapp.model.UserRoutesPOJO;
import com.podkidyshev.ticketpayapp.repository.RouteOrderRepository;
import com.podkidyshev.ticketpayapp.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;
    private  final RouteOrderRepository routeOrderRepository;

    public UserService(UserRepository userRepository, RouteOrderRepository routeOrderRepository) {
        this.userRepository = userRepository;
        this.routeOrderRepository = routeOrderRepository;
    }

    public UserRoutesPOJO getOne(Long id) throws UserNotFoundException {
        UserEntity userEntity = userRepository.findById(id).get();
        if (userEntity == null) {
            throw new UserNotFoundException("Пользователь не найден!");
        }
        return UserRoutesPOJO.toModel(userEntity);
    }


    public List<UserPOJO> findAll() {
        List<UserPOJO> allUsers = userRepository.findAll()
                .stream().map(UserPOJO::toModel)
                .collect(Collectors.toList());
       return  allUsers;
    }

    public UserPOJO registration(UserEntity userEntity) throws UserAlreadyExistsException {
        if (userRepository.findByEmail(userEntity.getEmail()) != null) {
            throw new UserAlreadyExistsException("Проверьте корректность введенных данных!");
        }
       return UserPOJO.toModel(userRepository.save(userEntity));
    }

    public UserPOJO edit(Long id, UserEditPOJO userEditPOJO) throws UserNotFoundException, IncorrectPasswordException {
        UserEntity userEntity  = userRepository.findById(id).get();
        if(userEntity == null) {
            throw new UserNotFoundException("Пользователь не найден!");
        }
        if(!userEntity.getPassword().equals(userEditPOJO.getPassword())) {
            throw  new IncorrectPasswordException("Пароль не верный");
        }
        userEntity.setFirstName(userEditPOJO.getFirstName());
        userEntity.setLastName(userEditPOJO.getLastName());
        userEntity.setEmail(userEditPOJO.getEmail());

        userRepository.save(userEntity);
        return UserPOJO.toModel(userEntity);
    }

    public String delete(Long id) {
        userRepository.deleteById(id);
        return "Пользователь "  + id + " был успешно удален.";
    }
}
