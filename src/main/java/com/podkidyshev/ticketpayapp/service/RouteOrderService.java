package com.podkidyshev.ticketpayapp.service;

import com.podkidyshev.ticketpayapp.entity.RouteOrderEntity;
import com.podkidyshev.ticketpayapp.entity.UserEntity;
import com.podkidyshev.ticketpayapp.exception.UserNotFoundException;
import com.podkidyshev.ticketpayapp.model.RouteOrderPOJO;
import com.podkidyshev.ticketpayapp.repository.RouteOrderRepository;
import com.podkidyshev.ticketpayapp.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RouteOrderService {

    private final RouteOrderRepository routeOrderRepository;
    private final UserRepository userRepository;

    public RouteOrderService(RouteOrderRepository routeOrderRepository, UserRepository userRepository) {
        this.routeOrderRepository = routeOrderRepository;
        this.userRepository = userRepository;
    }

    public List<RouteOrderPOJO> getAll() {
       return routeOrderRepository.findAll().stream().map(RouteOrderPOJO:: toModel).collect(Collectors.toList());
    }

    public RouteOrderPOJO getOne(Long id) {
        RouteOrderPOJO routeOrderPOJO = RouteOrderPOJO.toModel(routeOrderRepository.findById(id).get());
        return routeOrderPOJO;
    }

    public RouteOrderPOJO createNewOrder(RouteOrderEntity routeOrderEntity, Long userId) throws UserNotFoundException {
        UserEntity userEntity = userRepository.getById(userId);
        if(userEntity == null) {
            throw new UserNotFoundException("Пользователь не найден!");
        }
        routeOrderEntity.setUserEntity(userEntity);
        return RouteOrderPOJO.toModel(routeOrderRepository.save(routeOrderEntity));
    }

    public String delete(Long id) {
        routeOrderRepository.deleteById(id);
        return "Запись " + id + " была успешно удалена.";
    }

    @Transactional
    public String deleteAllByUser(Long id) throws UserNotFoundException {
       UserEntity userEntity =  userRepository.findById(id).get();
       if(userEntity == null) {
           throw new UserNotFoundException("Пользователь не найден!");
       }
       routeOrderRepository.deleteAllByUserEntity(userEntity);
       return "Все поездки пользователя " + id + " были удалены";
    }
}
